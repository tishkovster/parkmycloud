import time

class TestExample:
    
    
    def test_list_resources(self, client):
        res = client._s.get("https://console.parkmycloud.com/resources/paged?provider=aws")
        assert res.status_code == 200
        
    
    
    def test_get_resource(self, client):
        res = client._s.get("https://console.parkmycloud.com/resources/detail/9765505")
        assert res.status_code == 200
        
    
    
    def test_toggle_resouce(self, client):
        off_state = client._s.get("https://console.parkmycloud.com/resources/detail/9765505").json()
        toggle_data = {"item_ids": [9765505] , "action": "start"}
        res = client._s.put("https://console.parkmycloud.com/resources/toggle", json=toggle_data )
        time.sleep(10)
        on_state = client._s.get("https://console.parkmycloud.com/resources/detail/9765505").json()
        assert res.status_code == 202
        assert off_state['data']['instance_state'] != on_state['data']['instance_state'] 
        assert off_state['data']['state_reason'] != on_state['data']['state_reason']
    