**Steps to install:

git clone https://tishkovster@bitbucket.org/tishkovster/parkmycloud.git

cd parkmycloud

pip install -r requirements.txt

Modify file conftest.py with your key_id and key value.

    client.authorize("key_id", "key")

	
**Run with command: 

py.test tests -v --junitxml=result.xml