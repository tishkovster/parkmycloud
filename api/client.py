import requests
import json

class ParkMyCloudClient:

    _s = requests.session()
    host = None

    def __init__(self, host):
        self.host = host

    def login(self, key_id, key):
        data = {"key_id": key_id, "key": key }
        return self._s.post(self.host, json=data)
    
    def authorize(self, api_key_id, api_key):
        res = self.login(api_key_id, api_key)
        print(res.status_code)
        if res.status_code != 200:
            raise Exception(res.status_code)
        session_token = res.json().get("token")
        cookie = requests.cookies.create_cookie("token", session_token)
        self._s.cookies.set_cookie(cookie)

