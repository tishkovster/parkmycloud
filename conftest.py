import pytest


from api.client import ParkMyCloudClient


@pytest.fixture(scope="session")
def client():
    client= ParkMyCloudClient("https://console.parkmycloud.com/v2/auth/login")
    client.authorize("key_id", "key")
    return client